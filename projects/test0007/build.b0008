#!/bin/bash

# Original templates:
# https://gitlab.torproject.org/guest475646844/tor-browser-build/-/blob/maint-12.0/projects/android-components/build

set -v -x -e



[% c("var/set_default_env") -%]
[% pc(c('var/compiler'), 'var/setup', {
    compiler_tarfile => c('input_files_by_name/' _ c('var/compiler')),
    gradle_tarfile => c("input_files_by_name/gradle"),
  }) %]
distdir=/var/tmp/dist
builddir=/var/tmp/build/[% project %]
mkdir $distdir/[% project %]
mkdir /var/tmp/build

cat > get-moz-build-date << "EOF"
[% INCLUDE "get-moz-build-date" %]
EOF

[% IF !c("var/fetch_gradle_dependencies") %]
  tar -C $distdir -xf [% c('input_files_by_name/geckoview') %]
  gradle_repo=$rootdir/[% c('input_files_by_name/gradle-dependencies') %]
  # This overwrites the release, beta, and nightly geckoview .aar files in our
  # gradle-dependencies directory to be sure that only the one we provide is
  # getting used when building beta/release code.
  find $gradle_repo -type f -name geckoview*omni*.aar -exec cp -f $distdir/geckoview/geckoview*omni*.aar {} \;
  cp -r $gradle_repo/dl/android/maven2/* $gradle_repo
  cp -r $gradle_repo/maven2/* $gradle_repo
  cp -r $gradle_repo/m2/* $gradle_repo
  mkdir /home/rbm/.m2
  ln -s $gradle_repo /home/rbm/.m2/repository
  # Put the Kotlin/Native compiler at the right place, see:
  # tor-browser-build#40217.
  kotlin_dir=/home/rbm/.konan
  mkdir $kotlin_dir
  find $gradle_repo -type f -name kotlin-native-prebuilt-linux*tar.gz -exec tar -C $kotlin_dir -xaf {} \;
  tar -C $distdir -xf [% c('input_files_by_name/application-services') %]
  # XXX: We could be smarter and just copy over the projects that are needed
  # according to the Gradle dependencies list.
  cp -rf $distdir/application-services/maven/org $gradle_repo
[% END %]
tar -C /var/tmp/build -xf [% project %]-[% c('version') %].tar.gz

cd $builddir-[% c('version') %]

# Avoid call to `getGitHash()`, which will fail b/c it invokes git, which is absent in
# the build container. Instead just inject the commit hash, which we know!
sed -i 's/getGitHash()/\"[% c('abbrev') %]\"/' components/support/base/build.gradle
sed -i 's/tag = getGitHash()/tag = \"[% c('abbrev') %]\"/' publish.gradle


