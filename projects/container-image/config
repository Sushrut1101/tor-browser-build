# vim: filetype=yaml sw=2
filename: 'container-image_[% c("var/container/suite") %]-[% c("var/container/arch") %]-[% sha256(c("pre")).substr(0, 12) %].tar.gz'
pkg_type: build
container:
  use_container: 1

var:
  container:
    suite: '[% pc(c("origin_project"), "var/container/suite", { step => c("origin_step") }) %]'
    arch: '[% pc(c("origin_project"), "var/container/arch", { step => c("origin_step") }) %]'

lsb_release:
  id: Debian
  codename: jessie
  release: 8.11

targets:
  no_containers:
    filename: containers_disabled
    pre: ''
    input_files: []
    build: |
      mkdir -p [% dest_dir %]
      touch [% dest_dir _ '/' _ c('filename') %]

pre: |
  #!/bin/sh
  # Version: 2
  # [% c('var/container/suite') %]
  set -e
  export DEBIAN_FRONTEND=noninteractive
  # Update the package cache so the container installs the most recent
  # version of required packages.
  apt-get update -y -q
  [% IF pc(c('origin_project'), 'var/pre_pkginst', { step => c('origin_step') }) -%]
  [% pc(c('origin_project'), 'var/pre_pkginst', { step => c('origin_step') }) %]
  [% IF c("var/linux-cross") -%]
    dpkg --add-architecture [% c("var/arch_debian") %]
  [% END -%]
  [% IF c("var/container/suite") == "jessie" -%]
    # We need to use faketime to run `apt-get update` on jessie, because of
    # expired key. See tor-browser-build#40693
    dpkg -i ./libfaketime_0.9.6-3_amd64.deb ./faketime_0.9.6-3_amd64.deb
  [% END -%]
  # Update the package cache again because `pre_pkginst` may change the
  # package manager configuration.
  [% IF c("var/container/suite") == "jessie" %]faketime '2018-12-24 08:15:42' [% END %]apt-get update -y -q
  [% END -%]
  apt-get upgrade -y -q
  [%
     deps = [];
     IF pc(c('origin_project'), 'var/deps', { step => c('origin_step') });
       CALL deps.import(pc(c('origin_project'), 'var/deps', { step => c('origin_step') }));
     END;
     IF pc(c('origin_project'), 'var/arch_deps', { step => c('origin_step') });
       CALL deps.import(pc(c('origin_project'), 'var/arch_deps', { step => c('origin_step') }));
     END;
     IF deps.size;
       IF pc(c('origin_project'), 'var/sort_deps', { step => c('origin_step') });
         deps = deps.sort;
       END;
       FOREACH pkg IN deps;
         SET p = tmpl(pkg);
         IF p;
           GET c('install_package', { pkg_name => p });
           GET "\n";
         END;
       END;
     END;
  -%]
  [% IF pc(c('origin_project'), 'var/post_pkginst', { step => c('origin_step') }) -%]
  [% pc(c('origin_project'), 'var/post_pkginst', { step => c('origin_step') }) %]
  [% END -%]
  apt-get clean

remote_get: |
  #!/bin/sh
  set -e
  [%
    SET src = shell_quote(c('get_src', { error_if_undef => 1 }));
    SET dst = shell_quote(c('get_dst', { error_if_undef => 1 }));
  -%]
  mkdir -p "[% dst %]"
  [% c("rbmdir") %]/container archive '[% c("container/dir") %]' "[% dst %]/[% c("filename") %]"

input_files:
  - project: mmdebstrap-image
    target:
      - '[% c("var/container/suite") %]-[% c("var/container/arch") %]'
  - URL: http://deb.debian.org/debian/pool/main/f/faketime/faketime_0.9.6-3_amd64.deb
    sha256sum: 19b2a01a2fae7e6d5a8b741fc0bc626451cb4c2cc884ee79f1136dd3c2c26213
    enable: '[% c("var/container/suite") == "jessie" %]'
  - URL: http://deb.debian.org/debian/pool/main/f/faketime/libfaketime_0.9.6-3_amd64.deb
    sha256sum: 82747d5815b226cfed7f6f9a751bf8c20d457f3ba786add6017d6904dea4fdb4
    enable: '[% c("var/container/suite") == "jessie" %]'
